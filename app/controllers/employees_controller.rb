class EmployeesController < ApplicationController
	before_action :authenticate_model!
	def index
		@employees=Employee.all
	end
	def show
		@employee=Employee.find(params[:id])
	end
	def new
		@employee=Employee.new
	end
	def create
		@employee= Employee.new(employee_params)
        if @employee.save
        	redirect_to :action=>'index'
        end
        
	end
	private
	def employee_params
		params.require(:employee).permit(:name,:date_of_join,:designation,:registration_id)
	end
end
