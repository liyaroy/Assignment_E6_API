class Api::V1::SessionsController < Api::V1::BaseController
  def create
    employee = Employee.find_by(username: create_params[:username])
    if employee && employee.valid_password?(create_params[:password])
      # employee.update_attributes(notification_key: create_params[:notification_key])
      @employee = employee
      params[:current_employee] = @employee
      render(
         json: SessionSerializer.new(employee, root: false, params: params).to_json,
        status: 201
      )
    else
      render(
         json: { message: 'Incorrect username/password'},
        status: 401
      )
    end
  end

  private
  def create_params
    params.permit(:username, :password)
  end
end
