class Api::V1::EmployeesController < Api::ApiController
	def index
		employee=Employee.all
		render json: { data: employee}
	end

	def show
		employee=Employee.find(params[:id])
		render json: { data: employee}
	end
	
	def create
		@employee =Employee.create(employee_params)
    	errors = @employee.errors
    	if errors.blank?
        	@employee.reload
      		render :status => :ok, :json => {:employee => @employee,:message => ("employee succesfully added")}
    	else
      		render :status => :not_found, :json => {:message => errors.full_messages}
    	end
  	end
  	private
  	def employee_params
  		employee_params = params.require(:employee).permit(:name,:date_of_join,:designation,:registration_id)
	end
end

