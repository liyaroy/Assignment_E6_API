class TeamsController < ApplicationController
	before_action :authenticate_model!
	# def add_employees
	# 	@employees=Employee.all
	# end
	def index
		@teams=Team.all
	end
	def new
		@team=Team.new
	end
	def create
		@team=Team.new(team_params)
		if @team.save
			redirect_to :action=>'index'
		end
	end
	def show
		@team=Team.find(params[:id])
	end
	
	def add_employees
      	if params[:team_id]
      		puts params
        	@team = Team.find params[:team_id]
        	@employee = Employee.find params[:employee][:id]
        	@team.employees << @employee
        	redirect_to :action=>'index'
		end
	end
	private
	def team_params
		params.require(:team).permit(:name)
	end
	
end
