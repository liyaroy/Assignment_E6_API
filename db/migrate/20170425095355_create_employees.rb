class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :name
      t.date :date_of_join
      t.string :designation
      t.string :registration_id
      t.timestamps null: false
    end
  end
end
